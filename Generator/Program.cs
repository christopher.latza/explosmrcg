﻿using explosmrcg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Program
    {
        public static List<Bitmap> m_images = new List<Bitmap>();
        public static Bitmap m_layout = null;

        private static void copyImageInImage(ref Bitmap _template, ref Bitmap _image, Vector2 _position)
        {
            for (int y = 0; y < _image.Height; y++)
            {
                for (int x = 0; x < _image.Width; x++)
                {
                    _template.SetPixel(_position.X + x, _position.Y + y, _image.GetPixel(x, y));
                }
            }
        }

        private static Bitmap createImage(Bitmap _a, Bitmap _b, Bitmap _c)
        {
            Bitmap _ausgabe = new Bitmap(m_layout);

            copyImageInImage(ref _ausgabe, ref _a, new Vector2(4, 4));
            copyImageInImage(ref _ausgabe, ref _b, new Vector2(249, 4));
            copyImageInImage(ref _ausgabe, ref _c, new Vector2(494, 4));

            return _ausgabe;
        }

        static void Main(string[] args)
        {
            if (File.Exists("layout.png"))
                m_layout = new Bitmap("layout.png");

            if (!Directory.Exists("output"))
                Directory.CreateDirectory("output");

            if (Directory.Exists("finish"))
            {
                foreach (String _file in Directory.GetFiles("finish"))
                {
                    m_images.Add(new Bitmap(_file));
                }

                int A = 0;
                int B = 0;
                int C = 0;

                foreach (Bitmap _imageA in m_images)
                {
                    A = A + 1;
                    B = 0;
                    C = 0;

                    foreach (Bitmap _imageB in m_images)
                    {
                        B = B + 1;
                        C = 0;

                        foreach (Bitmap _imageC in m_images)
                        {
                            C = C + 1;

                            if (A != B && B != C && A != C)
                            {
                                Bitmap _new = createImage(_imageA, _imageB, _imageC);
                                _new.Save("output/" + A + "-" + B + "-" + C + ".png");
                                _new.Dispose();
                            }
                        }
                    }
                }
            }
        }
    }
}
