﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace explosmrcg
{
    class rcg
    {
        private static Bitmap m_removeImage = null;

        static void Main(string[] args)
        {
            if (File.Exists("plain.png"))
                m_removeImage = new Bitmap("plain.png");

            if (!Directory.Exists("parts/"))
                Directory.CreateDirectory("parts/");

            if (!Directory.Exists("finish/"))
                Directory.CreateDirectory("finish/");

            while (true)
            {
                try
                {
                    String _imageURL = WebTools.getImageLink();
                    Bitmap _image = WebTools.getImage(_imageURL);

                    ImageTools.removeAllSamePixels(ref _image, ref m_removeImage);
                    List<Bitmap> _images = ImageTools.getSubImagesFromMainImage(_image);

                    foreach(Bitmap _b in _images)
                    {
                        String _name = ImageTools.getName(_b);

                        if (!Directory.Exists(Path.Combine("parts/", _name + "/")))
                            Directory.CreateDirectory(Path.Combine("parts/", _name + "/"));

                        String _counter = Path.Combine("parts/", _name + "/") + "count.txt";
                        String _path = Path.Combine("parts/", _name + "/") + "full.png";
                        String _finish = Path.Combine("finish/", _name + ".png");

                        if (File.Exists(_path))
                        {
                            int _count = 1;

                            if (File.Exists(_counter))
                                _count = int.Parse(File.ReadAllText(_counter));

                            Console.WriteLine("Download version " + ++_count + " from image " + _name);

                            Bitmap _fullImage = new Bitmap(_path);
                            ImageTools.combineImages(ref _fullImage, _b);

                            MemoryStream _temp = new MemoryStream();

                            _fullImage.Save(_temp, ImageFormat.Png);
                            _fullImage.Dispose();

                            Bitmap _tempImage = new Bitmap(_temp);
                            _tempImage.Save(_path);

                            if (_count > 5)
                                File.Copy(_path, _finish, true);

                            File.WriteAllText(_counter, "" + _count);
                        }
                        else
                        {
                            Console.WriteLine("Download version 1 from image " + _name);
                            _b.Save(_path);
                        }

                        _b.Dispose();
                    }
                }
                catch (Exception _error)
                {
                    Console.WriteLine("Error");
                    Console.WriteLine(_error.Message);
                    Console.WriteLine(_error.StackTrace);
                }
            }
        }
    }
}
