﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace explosmrcg
{
    public class Vector2
    {
        public int X = 0;
        public int Y = 0;

        public Vector2(int _x, int _y)
        {
            X = _x;
            Y = _y;
        }
    }
}
