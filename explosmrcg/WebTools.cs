﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace explosmrcg
{
    class WebTools
    {
        private static String m_baseURL = "http://explosm.net/rcg/view/";
        private static WebClient m_client = null;

        private static WebClient getWebClient()
        {
            WebClient _return = new WebClient();

            _return.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            IWebProxy _proxy = WebRequest.DefaultWebProxy;
            if (_proxy != null)
            {
                _proxy.Credentials = CredentialCache.DefaultCredentials;
                _return.Proxy = _proxy;
            }

            return _return;
        }

        public static String getImageLink()
        {
            if (m_client == null)
                m_client = getWebClient();

            String _fileContend = m_client.DownloadString(m_baseURL);

            HtmlDocument _HTMLContend = new HtmlDocument();
            _HTMLContend.LoadHtml(_fileContend);

            return "http:" + _HTMLContend.DocumentNode.SelectNodes("//div/img").First().Attributes["src"].Value;
        }

        public static Bitmap getImage(String _url)
        {
            if (m_client == null)
                m_client = getWebClient();

            byte[] _fileContend = m_client.DownloadData(_url);

            MemoryStream _stream = new MemoryStream(_fileContend);
            Image image = Image.FromStream(_stream);
            return (Bitmap)image;
        }
    }
}
