﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace explosmrcg
{
    class ImageTools
    {
        private static List<Vector2> m_startPointList = new List<Vector2>(new Vector2[] { new Vector2(4, 4), new Vector2(249, 4), new Vector2(494, 4) });

        public static List<Bitmap> getSubImagesFromMainImage(Bitmap _mainImage)
        {
            List<Bitmap> _return = new List<Bitmap>();

            foreach (Vector2 _startPosition in m_startPointList)
            {
                Bitmap _new = new Bitmap(232, 272);

                int _relY = 0;
                for (int y = _startPosition.Y; y < _startPosition.Y + 272; y++)
                {
                    int _relX = 0;
                    for (int x = _startPosition.X; x < _startPosition.X + 232; x++)
                    {
                        if ((_relX >= 0 && _relY >= 0 && _relX <= _new.Size.Width && _relY <= _new.Size.Height) && (x >= 0 && y >= 0 && x <= _mainImage.Size.Width && y <= _mainImage.Size.Height))
                            _new.SetPixel(_relX, _relY, _mainImage.GetPixel(x, y));

                        _relX++;
                    }

                    _relY++;
                }

                _return.Add(_new);
            }

            return _return;
        }

        public static void removeAllSamePixels(ref Bitmap _image, ref Bitmap _remove)
        {
            if (_image.Size.Width == _remove.Size.Width && _image.Size.Height == _remove.Size.Height)
            {
                for (int y = 0; y < _image.Height; y++)
                {
                    for (int x = 0; x < _image.Width; x++)
                    {
                        if (_image.GetPixel(x, y).ToArgb() == _remove.GetPixel(x, y).ToArgb())
                            _image.SetPixel(x, y, Color.White);
                    }
                }
            }
        }

        public static void combineImages(ref Bitmap _image, Bitmap _add)
        {
            if (_image.Size.Width == _add.Size.Width && _image.Size.Height == _add.Size.Height)
            {
                for (int y = 0; y < _image.Height; y++)
                {
                    for (int x = 0; x < _image.Width; x++)
                    {
                        if(_image.GetPixel(x, y).ToArgb() != _add.GetPixel(x, y).ToArgb() && _add.GetPixel(x, y).ToArgb() != Color.White.ToArgb())
                        {
                            _image.SetPixel(x, y, _add.GetPixel(x, y));
                        } 
                    }
                }
            }
        }

        public static string getName(Bitmap _image)
        {
            return ImageTools.getImageColorHash(ref _image, 15);
        }

        /// <summary>
        /// Gibt einen MD5 Hash als String zurück
        /// </summary>
        /// <param name="TextToHash">string der Gehasht werden soll.</param>
        /// <returns>Hash als string.</returns>
        public static string GetMD5Hash(string TextToHash)
        {
            //Prüfen ob Daten übergeben wurden.
            if ((TextToHash == null) || (TextToHash.Length == 0))
            {
                return string.Empty;
            }

            //MD5 Hash aus dem String berechnen. Dazu muss der string in ein Byte[]
            //zerlegt werden. Danach muss das Resultat wieder zurück in ein string.
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] textToHash = Encoding.Default.GetBytes(TextToHash);
            byte[] result = md5.ComputeHash(textToHash);

            return System.BitConverter.ToString(result);
        }

        public static String getImageColorHash(ref Bitmap _image, int _areaSize)
        {
            String _hash = "";

            for (int y = (_areaSize / 2); y < _image.Height; y = y + _areaSize)
            {

                if(y <= 260)
                {
                    for (int x = (_areaSize / 2); x < _image.Width; x = x + _areaSize)
                    {
                        _hash += _image.GetPixel(x, y).Name;
                        //_image.SetPixel(x, y, Color.Red);
                    }
                }
            }


            return GetMD5Hash(_hash).Replace("-", "");

        }
    }
}
